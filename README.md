<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# kivy_user_prefs 0.3.34

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_user_prefs/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_user_prefs)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_user_prefs/release0.3.33?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_user_prefs/-/tree/release0.3.33)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_user_prefs)](
    https://pypi.org/project/ae-kivy-user-prefs/#history)

>ae_kivy_user_prefs package 0.3.34.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_user_prefs/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_user_prefs/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_user_prefs/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_user_prefs/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_user_prefs/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_user_prefs/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_user_prefs)](
    https://gitlab.com/ae-group/ae_kivy_user_prefs/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_user_prefs)](
    https://gitlab.com/ae-group/ae_kivy_user_prefs/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_user_prefs)](
    https://gitlab.com/ae-group/ae_kivy_user_prefs/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_user_prefs)](
    https://pypi.org/project/ae-kivy-user-prefs/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_user_prefs)](
    https://gitlab.com/ae-group/ae_kivy_user_prefs/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_user_prefs)](
    https://libraries.io/pypi/ae-kivy-user-prefs)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_user_prefs)](
    https://pypi.org/project/ae-kivy-user-prefs/#files)


## installation


execute the following command to install the
ae.kivy_user_prefs package
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-user-prefs
```

if you want to contribute to this portion then first fork
[the ae_kivy_user_prefs repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_user_prefs "ae.kivy_user_prefs code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_user_prefs):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_user_prefs/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_user_prefs.html
"ae_kivy_user_prefs documentation").
